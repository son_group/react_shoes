import { Component } from "react";
import Shoes_Item from "./Shoes_Item";

export default class List_Shoes extends Component {
  renderShoesList = () => {
    // let { shoesArr } = this.props.shoesArr;
    return this.props.shoesArr.map((shoes, index) => {
      return (
        <div className="col-4" key={index}>
          <Shoes_Item shoesItem={shoes} />
        </div>
      );
    });
  };

  render() {
    return (
      <div className="container">
        <div className="row">{this.renderShoesList()}</div>
      </div>
    );
  }
}
