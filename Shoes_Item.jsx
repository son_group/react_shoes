import { Component } from "react";

export default class Shoes_Item extends Component {
  render() {
    let { name, price, shortDescription, image } = this.props.shoesItem;
    // console.log("name ", this.props.Shoes_Item);
    return (
      <div>
        <div>
          <div
            className="card m-2 "
            style={{ width: "18rem", height: "28rem" }}
          >
            <img
              className="card-img-top rounded mx-auto d-block "
              src={image}
              alt="Card image cap"
              style={{ height: "15rem", width: "auto" }}
            />
            <div className="card-body">
              <h5 className="card-title">{name}</h5>
              <p className="card-text">
                {shortDescription < 40
                  ? shortDescription
                  : shortDescription.substring(0, 40)}
              </p>
              <button className="btn btn-primary mr-3">Shoes detail</button>
              <button className="btn btn-secondary">Add to cart</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
