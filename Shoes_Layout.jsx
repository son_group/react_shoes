import { Component } from "react";
import { dataShoes } from "./data";
import Header from "./Header";
import List_Shoes from "./List_Shoes";

export default class Shoes_Layout extends Component {
  state = {
    shoesArr: dataShoes,
    shoesDetail: {},
    shoesCart: [],
  };

  render() {
    return (
      <div>
        <Header />
        <List_Shoes shoesArr={this.state.shoesArr} />
      </div>
    );
  }
}
